package ru.t1.dzelenin.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();
}
