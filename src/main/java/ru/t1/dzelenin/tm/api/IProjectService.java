package ru.t1.dzelenin.tm.api;

import ru.t1.dzelenin.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);
}
