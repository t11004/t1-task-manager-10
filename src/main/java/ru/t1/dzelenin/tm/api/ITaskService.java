package ru.t1.dzelenin.tm.api;

import ru.t1.dzelenin.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);
}
